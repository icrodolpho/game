package com.ivacrod.game.logs;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class ProdutorLog {

	private static final String TOPIC = "game";
	private static final String SERVERS = "10.162.107.229:9090;10.162.107.229:9091;10.162.107.229:9092";

	/**
	 * 
	 * @param index
	 * @return
	 */
	public static KafkaProducer<String, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "Produtor-Game-A");
		// props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		KafkaProducer<String, String> producer = new KafkaProducer<>(props);
		return producer;
	}

	public void sendLog(String mensagem) {
//	public static Thread createProducerThread(String mensagem) {
//		Thread t = new Thread(new Runnable() {

//			@Override
//			public void run() {
		System.out.printf("Thread [%s] starting.\n", Thread.currentThread().getName());
		Random random = new Random();
		KafkaProducer<String, String> producer = createProducer();

		final String topico = TOPIC;
		// final long key = random.nextInt(10000000);
		final String key = "a.log." + String.valueOf((random.nextInt(10000000)));
		final String body = mensagem;

		ProducerRecord<String, String> record = new ProducerRecord<>(topico, key, body);

		try {
			producer.send(record).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		producer.flush();
		producer.close();
	}
//		});
//		t.setName("ProducerThread-Game-A");
//		return t;
//	}

//	public void sendLog(String mensagem) {
//
//		Thread t = createProducerThread(mensagem);
//		t.start();
//	}
}
