package com.ivacrod.game.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import com.ivacrod.game.models.Pergunta;

@Service
public class PerguntasService {

	private RestTemplate restTemplate = new RestTemplate();

	public List<Pergunta> getPerguntasHTTP(int qtdePerguntas) {
		// String url = "http://10.162.106.158:8000/random/" + qtdePerguntas;
		String url = "http://10.162.110.150:8080/pergunta/" + qtdePerguntas;

		ResponseEntity<List<Pergunta>> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Pergunta>>() {
				});

		List<Pergunta> listaPerguntas = response.getBody();

		return listaPerguntas;
	}

	public List<Pergunta> getPerguntasMQ(int qtdePerguntas) {
		try {

			// Abrir conexão com o broker
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://amq.oramosmarcos.com:61616");
			Connection connection = connectionFactory.createConnection();
			connection.start();

			// Criar uma sessão
			Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

			// A fila que queremos popular
			Queue queue = session.createQueue("a.queue.questions.full");

			// Nosso produtor de eventos
			MessageProducer producer = session.createProducer(queue);

			// Criar uma fila temporária para receber a mensagem de resposta
			Destination tempQueue = session.createTemporaryQueue();

			// Criar o consumidor da fila temporária
			MessageConsumer responseConsumer = session.createConsumer(tempQueue);

			// Construir a mensagem
			MapMessage msg = session.createMapMessage();
			msg.setInt("quantity", qtdePerguntas);
			msg.setString("random", "yes");

			// Configurar os parâmetros de resposta
			msg.setJMSCorrelationID("quantity" + qtdePerguntas);
			msg.setJMSReplyTo(tempQueue);

			// Enviar a mensagem
			producer.send(msg);
			System.out.println("Sent: " + msg.getJMSMessageID());

			// Get the response
			MapMessage response = (MapMessage) responseConsumer.receive(100000);
			response.getObject("questions");

			

			List<Pergunta> listaPerguntas = new ArrayList();
			List<HashMap<String, String>> questionsResponse = new ArrayList<HashMap<String, String>>();
			//HashMap<String, String> questionResponse = new HashMap<String, String>();
			questionsResponse = (ArrayList<HashMap<String, String>>) response.getObject("questions");
			

			for (HashMap<String, String> hashMap : questionsResponse) {
				Pergunta pergunta = new Pergunta();
				pergunta.setCategoria(hashMap.get("category"));
				pergunta.setOpcao1(hashMap.get("option1"));
				pergunta.setOpcao2(hashMap.get("option2"));
				pergunta.setOpcao3(hashMap.get("option3"));
				pergunta.setOpcao4(hashMap.get("option4"));
				pergunta.setResposta(Integer.parseInt(hashMap.get("answer")));
				pergunta.setTitulo(hashMap.get("title"));
				
				listaPerguntas.add(pergunta);
			}
			

			msg.acknowledge();
			responseConsumer.close();
			
			return listaPerguntas;

		} catch (JMSException ex) {
			ex.printStackTrace();
			
			return null;
		}

	}

}
