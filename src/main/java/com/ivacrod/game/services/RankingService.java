package com.ivacrod.game.services;

import java.util.Random;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ivacrod.game.models.Ranking;

@Service
public class RankingService {

	@Autowired
	private JmsTemplate jmsTemplate;

	private RestTemplate restTemplate = new RestTemplate();

	public Ranking setRankingHTTP(String nomeJogo, String NomeJogador, int pontuacao) {
		String url = "http://10.162.110.213:8080/ranking";

		Ranking ranking = new Ranking();
		ranking.setNomeJogador(NomeJogador);
		ranking.setNomeJogo(nomeJogo);
		ranking.setPontuacao(pontuacao);

		return restTemplate.postForObject(url, ranking, Ranking.class);
	}

//	public void setRankingMQ(String nomeJogo, String NomeJogador, int pontuacao) {
//
//		HashMap<String, String> body = new HashMap<>();
//		body.put("nomeJogador", NomeJogador);
//		body.put("nomeJogo", nomeJogo);
//		body.put("pontuacao", Integer.toString(pontuacao));
//
//		jmsTemplate.convertAndSend("a.queue.ranking", body);
//
//		//return new Ranking();
//	}

	public void setRankingMQ(String nomeJogo, String NomeJogador, int pontuacao, int qtdePerguntas) {
		try {

			// Abrir conexão com o broker
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://amq.oramosmarcos.com:61616");
			Connection connection = connectionFactory.createConnection();
			connection.start();

			// Criar uma sessão
			Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

			// A fila que queremos popular
			Queue queue = session.createQueue("a.queue.ranking");

			// Nosso produtor de eventos
			MessageProducer producer = session.createProducer(queue);

			MapMessage msg = session.createMapMessage();
			msg.setString("playerName", NomeJogador);
			msg.setString("gameId", nomeJogo);
			msg.setString("hits", String.valueOf(pontuacao));
			msg.setString("total", String.valueOf(qtdePerguntas));
			producer.send(msg);
			
			System.out.println("Sent: " + msg.getJMSMessageID());

		} catch (JMSException ex) {
			ex.printStackTrace();
		}

	}

}
