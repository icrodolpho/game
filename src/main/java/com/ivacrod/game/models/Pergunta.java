package com.ivacrod.game.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pergunta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String titulo;
	private String categoria;
	private String opcao1;
	private String opcao2;
	private String opcao3;
	private String opcao4;
	private int resposta;
	private int respostaUsuario;
	
	public Pergunta() {
		
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getOpcao1() {
		return opcao1;
	}
	public void setOpcao1(String opcao1) {
		this.opcao1 = opcao1;
	}
	public String getOpcao2() {
		return opcao2;
	}
	public void setOpcao2(String opcao2) {
		this.opcao2 = opcao2;
	}
	public String getOpcao3() {
		return opcao3;
	}
	public void setOpcao3(String opcao3) {
		this.opcao3 = opcao3;
	}
	public String getOpcao4() {
		return opcao4;
	}
	public void setOpcao4(String opcao4) {
		this.opcao4 = opcao4;
	}
	public int getResposta() {
		return resposta;
	}
	public void setResposta(int resposta) {
		this.resposta = resposta;
	}
	public int getRespostaUsuario() {
		return respostaUsuario;
	}
	public void setRespostaUsuario(int respostaUsuario) {
		this.respostaUsuario = respostaUsuario;
	}
		
}
