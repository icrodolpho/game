package com.ivacrod.game.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Jogo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private String nomeUsuario; 
	
	@NotNull
	private int qtdePerguntas;
	
	@NotNull
	private int qtdeAcertos;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<Pergunta> listaPerguntas;
	
	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNomeUsuario() {
		return nomeUsuario;
	}


	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}


	public int getQtdePerguntas() {
		return qtdePerguntas;
	}


	public void setQtdePerguntas(int qtdePerguntas) {
		this.qtdePerguntas = qtdePerguntas;
	}


	public int getQtdeAcertos() {
		return qtdeAcertos;
	}


	public void setQtdeAcertos(int qtdeAcertos) {
		this.qtdeAcertos = qtdeAcertos;
	}


	public List<Pergunta> getListaPerguntas() {
		return listaPerguntas;
	}


	public void setListaPerguntas(List<Pergunta> listaPerguntas) {
		this.listaPerguntas = listaPerguntas;
	}
	

}
